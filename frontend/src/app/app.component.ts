import { Component } from '@angular/core';
import { HttpCallsService } from 'src/services/http-calls.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [HttpCallsService]
})
export class AppComponent {
  title = 'frontend';
  constructor(private httpCalls: HttpCallsService) { }
  ngOnInit() {
    this.httpCalls.get('https://my-json-server.typicode.com/').subscribe(res => console.log('resss ========> ', res))
  }
}
