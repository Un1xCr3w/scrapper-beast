// > common imports
const express = require('express')
const app = express()
const mongoose = require('mongoose')
require('dotenv/config')
const bodyParser = require('body-parser')
const cors = require('cors')

// > Iniate the body parser
app.use(cors())
app.use(bodyParser.json())

// > Routes Imports
const homeRoute = require('./routes/home')
const itemsRoute = require('./routes/items')
const scrapRoute = require('./routes/scrapper')
const authRoute = require('./routes/auth')

// > Routes
app.use('/', homeRoute)
app.use('/items', itemsRoute)
app.use('/api/scrap', scrapRoute)
app.use('/api/user', authRoute)

// > Connect To DB Cluster
mongoose.connect(
    process.env.DB_CONNECTION,
    {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    }
)
    .then(() => console.log('DB Connected!'))
    .catch(err => {
        console.log(`DB Connection Error: ${err.message}`);
    });

// > Check If The Route Exist
app.use((req, res) => {
    res.send("This Route Isn't A Valid Route");
})

// > listen to the port
let port = process.env.PORT || 6000;
app.listen(port, () => {
    console.log(`server is up and running on port ${port}`)

})