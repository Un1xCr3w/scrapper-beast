const express = require('express');
const router = express.Router();
const userModel = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// > Valdation Imports
const { registerValidation, loginValidation } = require('../validation');

// > Verify Token Imports
const verify = require('../routes/verify-token');

//> Get All Users
router.get('/', verify, (req, res) => {
    userModel.find()
        .then(users => res.status(200).send({ data: users }))
})

// > Register New User
router.post('/register', (req, res) => {
    let hashedPassword;

    // > validation section
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //> Hash The Password
    bcrypt.genSalt(10)
        .then(salt => {
            bcrypt.hash(req.body.password, salt)
                .then(password => hashedPassword = password)
        })

    // > Check If User Exists
    userModel.findOne({ email: req.body.email })
        .then(email => {
            if (email == null) {
                // > Create User
                const user = new userModel({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: hashedPassword,
                })
                user.save()
                    .then(data => res.json({
                        data: {
                            id: data._id,
                            email: data.email
                        }
                    }))
                    .catch(err => res.json({ message: err }))
            } else {
                res.status(400).send('User Exists!')
            }
        })
})

// > User Login && Token Generator
router.post('/login', (req, res) => {
    // > validation section
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send({ message: error.details[0].message });
    userModel.findOne({ email: req.body.email })
        .then(user => {
            // > Check If The Email Is Correct
            if (user) {
                // > Compare Passwords Gather
                bcrypt.compare(req.body.password, user.password)
                    .then(validPassword => {
                        // > Check if the password is valid
                        if (validPassword) {

                            // > Create Token
                            const token = jwt.sign({ id: user._id }, process.env.TOKEN_SECRET);
                            res.header('auth-token', token).send(token)
                            console.log(token)
                        } else {
                            res.status(400).send({ message: 'Wrong Password' })
                        }
                    })
                    .catch(err => res.status(500).send({ message: err }))
            } else {
                res.status(400).send({ message: 'Wrong Email' })
            }


        })
})

module.exports = router;