const express = require('express');
const router = express.Router();

const ItemModal = require('../models/Item');

// > Get All Items

router.get('/', (req, res) => {
    ItemModal.find()
        .then(data => res.json({ data: data }))
        .catch(err => res.json({ message: err }))

})

// > Get Specific Item
router.get('/:itemId', (req, res) => {
    ItemModal.findById(req.params.itemId)
        .then(data => res.json({ data: data }))
        .catch(err => res.json({ message: err }))
})

// > Create New Item
router.post('/', (req, res) => {
    const item = new ItemModal({
        title: req.body.title,
        description: req.body.description,
    })

    item.save()
        .then(data => res.json({ data: data }))
        .catch(err => res.json({ message: err }))
})

// > Delete Specific Item
router.delete('/:itemId', (req, res) => {
    ItemModal.deleteOne({ _id: req.params.itemId })
        .then(data => res.json({ data: data }))
        .catch(err => res.json({ message: err }))
})

// > Delete All Items
router.delete('/', (req, res) => {
    ItemModal.deleteMany()
        .then(data => res.json({ data: data }))
        .catch(err => res.json({ message: err }))
})

// > Update Specific Item
router.patch('/:itemId', (req, res) => {
    ItemModal.updateOne({ _id: req.params.itemId }, {
        $set: {
            title: req.body.title,
            description: req.body.description,
        }
    })
        .then(data => res.json({ data: data }))
        .catch(err => res.json({ message: err }))
})

module.exports = router;